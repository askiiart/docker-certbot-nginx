#!/usr/bin/env bash
set -e
ORG=askiiart
NAME=certbot-nginx
ID=$(docker build . -q)

docker tag ${ID} ${ORG}/${NAME}:latest
docker push ${ORG}/${NAME}:latest
