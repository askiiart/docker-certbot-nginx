#!/bin/sh
if [ -z "$CERTBOT_DNS_PLUGIN" ]; then
    echo "No DNS plugin set, skipping"
else
    pip install $CERTBOT_DNS_PLUGIN
fi

sh -c $@
