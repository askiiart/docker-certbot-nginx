# [docker-certbot-nginx](https://git.askiiart.net/askiiart/docker-certbot-nginx)

[![Build Status](https://drone.askiiart.net/api/badges/askiiart/docker-certbot-nginx/status.svg?ref=refs/heads/main)](https://drone.askiiart.net/askiiart/docker-certbot-nginx)

`certbot` with `nginx` support in a Docker container

## Running

Example `docker run`:

```bash
docker run -d -v ./nginx-conf:/etc/nginx docker.askiiart.net/askiiart/certbot-nginx certbot --version
```

Example `docker-compose.yml`:

```yaml
version: '3.7'
services:
  hugo:
    image: docker.askiiart.net/askiiart/certbot-nginx
    volumes:
      - ./nginx-conf:/etc/nginx
    command: certbot --version
```

To add a DNS plugin, set the `CERTBOT_DNS_PLUGIN` environment variable to the name of the plugin to install. Plugins list [here](https://eff-certbot.readthedocs.io/en/latest/using.html#dns-plugins).

### Environment Variables

| Variable           | Description                   | Default |
| ------------------ | ----------------------------- | ------- |
| CERTBOT_DNS_PLUGIN | The certbot dns plugin to use | none    |

## Building

1. Run `docker build .`

## Some commands

Check if nginx config is valid:

```bash
docker run --rm -v ./nginx-conf:/etc/nginx docker.askiiart.net/askiiart/certbot-nginx nginx -t
```

First generate certs (from nginx configs):

```bash
docker run -it --rm -v ./nginx-conf:/etc/nginx docker.askiiart.net/askiiart/certbot-nginx certbot --nginx
```

Renew certs:

```bash
docker run -it --rm -v ./nginx-conf:/etc/nginx docker.askiiart.net/askiiart/certbot-nginx certbot renew
```
