FROM python:3.12-alpine
RUN pip install certbot certbot-nginx --no-cache-dir
RUN apk add nginx --no-cache
ADD run.sh /root/run.sh
CMD [ "/root/run.sh" ]